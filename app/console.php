<?php
require __DIR__ . '/../vendor/autoload.php';

use Command\CommandFactory;
use Symfony\Component\Console\Application;

if (!file_exists((__DIR__ . '/config/parameters.json'))) {
    throw new \Exception('Configure app/config/parameters.json');
}
$config = json_decode(file_get_contents(__DIR__ . '/config/parameters.json'), true);

$application = new Application();
$factory = new CommandFactory();
$cachePath = __DIR__ . DIRECTORY_SEPARATOR . 'cache';
$command = $factory->createCommand('create-empty-task-in-leads', $config, $cachePath);
$application->add($command);
$application->run();