<?php

namespace Service;

class AmoCrmApiService
{
    const GET_HTTP = 1;
    const POST_HTTP = 2;

    const API_DOMAIN = 'amocrm.ru';
    
    const API_AUTH_ENDPOINT       = '/private/api/auth.php?type=json';
    const API_LEADS_LIST_ENDPOINT = '/private/api/v2/json/leads/list';
    const API_TASKS_LIST_ENDPOINT = '/private/api/v2/json/tasks/list';
    const API_TASKS_SET_ENDPOINT  = '/private/api/v2/json/tasks/set';

    const API_TASK_CONTACT = 1;
    const API_TASK_LEAD = 2;

    const API_BATCH_SIZE = 100;

    /** @var  string */
    private $apiUser;

    /** @var  string */
    private $apiKey;

    /** @var  string */
    private $subDomain;

    /** @var  string */
    private $cachePath;

    /**
     * AmoCrmApiService constructor.
     * @param $apiUser
     * @param $apiKey
     * @param $subDomain
     * @param string $cachePath
     */
    public function __construct($apiUser, $apiKey, $subDomain, $cachePath = '/')
    {
        $this->apiUser = $apiUser;
        $this->apiKey = $apiKey;
        $this->subDomain = $subDomain;
        $this->cachePath = $cachePath;
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        $endpoint = $this->getEndpointFor(self::API_AUTH_ENDPOINT);
        $connection = $this->prepareConnection($endpoint, self::POST_HTTP);
        curl_setopt($connection, CURLOPT_POSTFIELDS, http_build_query($this->getUserAuthArray()));
        $res = $this->sendRequest($connection);

        return $res ? $res['response']['auth'] : $res;
    }

    /**
     * @return int[]
     */
    public function getAllLeadsIds()
    {
        $offset = 0;
        $ids = [];

        while ($items = $this->getDataFromApi(
            $this->getEndpointFor(self::API_LEADS_LIST_ENDPOINT),
            ['limit_rows' => self::API_BATCH_SIZE,'limit_offset' => $offset]
        )) {
            $ids = array_merge($ids, array_map(function ($lead) {
                return intval($lead['id']);
            }, $items['response']['leads']));

            $offset += self::API_BATCH_SIZE;
        }

        return $ids;
    }

    /**
     * @return int[]
     */
    public function getLeadsIdsWithLinkedTasks()
    {
        $offset = 0;
        $ids = [];
        while ($items = $this->getDataFromApi(
            $this->getEndpointFor(self::API_TASKS_LIST_ENDPOINT),
            ['element_type' => self::API_TASK_LEAD, 'limit_rows' => self::API_BATCH_SIZE,'limit_offset' => $offset]
        )) {
            $ids = array_merge($ids, array_map(function ($item) {
                return (int)$item['element_id'];
            }, $items['response']['tasks']));
            $offset += self::API_BATCH_SIZE;
        }

        return array_unique($ids);
    }

    /**
     * @param $leadsIds
     * @param string $emptyTaskText
     * @return bool|array
     */
    public function createEmptyTasksForLeads($leadsIds, $emptyTaskText = '')
    {
        $chunks = array_chunk($leadsIds, self::API_BATCH_SIZE);
        $result = [];
        foreach ($chunks as $idsChunk) {
            $data = $this->createEmptyTasksDataForLeads($idsChunk, $emptyTaskText);

            $endpoint = $this->getEndpointFor(self::API_TASKS_SET_ENDPOINT);
            $connection = $this->prepareConnection($endpoint, self::POST_HTTP);
            curl_setopt($connection, CURLOPT_POSTFIELDS, json_encode($data));

            $res = $this->sendRequest($connection);
            if ($res) {
                $result = array_merge($result, $res['response']['tasks']['add']);
            }
        }

        return $result ? $result : false;
    }

    /**
     * @param $leadsIds
     * @param string $emptyTaskText
     * @return mixed
     */
    private function createEmptyTasksDataForLeads($leadsIds, $emptyTaskText = '')
    {
        $data['request']['tasks']['add'] = [];
        foreach ($leadsIds as $id) {
            $data['request']['tasks']['add'][] = [
                'element_id' => $id,
                'element_type' => self::API_TASK_LEAD,
                'task_type' => 1,
                'text' => $emptyTaskText
            ];
        }

        return $data;
    }

    /**
     * @param $url
     * @param array $queryParams
     * @return array|bool
     */
    private function getDataFromApi($url, array $queryParams = [])
    {
        $params = '';
        foreach ($queryParams as $name => $value) {
            $params .= sprintf('%s=%s&', $name, $value);
        }
        if ($params) {
            $url .= '?' . rtrim($params, '&');
        }

        $connection = $this->prepareConnection($url, self::GET_HTTP);
        $res = $this->sendRequest($connection);

        return $res;
    }

    /**
     * @return string[]
     */
    private function getUserAuthArray()
    {
        return [
            'USER_LOGIN' => $this->apiUser,
            'USER_HASH' => $this->apiKey,
        ];
    }

    /**
     * @param $url
     * @return resource
     */
    private function prepareConnection($url, $httpMethod)
    {
        $connection = curl_init();
        curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($connection, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($connection, CURLOPT_URL, $url);
        if ($httpMethod === self::POST_HTTP) {
            curl_setopt($connection, CURLOPT_POST, true);
        }
        curl_setopt($connection, CURLOPT_HEADER, false);
        curl_setopt($connection, CURLOPT_COOKIEFILE, $this->cachePath . 'cookie');
        curl_setopt($connection, CURLOPT_COOKIEJAR, $this->cachePath . '/cookie');
        curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, 0);

        return $connection;
    }

    /**
     * @param resource $connection
     * @return bool
     */
    private function sendRequest($connection)
    {
        $out = curl_exec($connection);
        $code = curl_getinfo($connection, CURLINFO_HTTP_CODE);
        curl_close($connection);
        if ($code != 200 && $code != 204) {
            return false;
        }

        return @json_decode($out, true);
    }

    /**
     * @param $type
     * @return string
     */
    private function getEndpointFor($type)
    {
        return 'https://' . $this->subDomain . '.' . self::API_DOMAIN . $type;
    }

}