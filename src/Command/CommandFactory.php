<?php
namespace Command;

use Service\AmoCrmApiService;

/**
 * Class CreateEmptyTaskInLeadsWithoutOpenedTasksCommand
 * @package Command
 */
class CommandFactory
{
    /** commands */
    const CREATE_TASK_IN_LEADS = 'create-empty-task-in-leads';

    /**
     * @param $name
     * @param array $config
     * @param string $cachePath
     * @return CreateEmptyTaskInLeadsWithoutOpenedTasksCommand
     */
    public function createCommand($name, array $config, $cachePath = '/')
    {
        $cachePath = $this->validateCachePath($cachePath);
        switch($name) {
            case self::CREATE_TASK_IN_LEADS:
                $config = $config['amo_api'];
                $service = new AmoCrmApiService($config['user'], $config['key'], $config['sub_domain'], $cachePath);
                return new CreateEmptyTaskInLeadsWithoutOpenedTasksCommand($service);
            default:
        }
    }

    /**
     * @param $cachePath
     * @return string
     * @throws \Exception
     */
    public function validateCachePath($cachePath)
    {
        if(!is_dir($cachePath)) {
            throw new \Exception('Cache dir doesn\'t exists : ' . $cachePath);
        }

        return rtrim($cachePath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR;
    }
}