<?php
namespace Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Service\AmoCrmApiService;
use Command\Exception\AuthorizeException;

/**
 * Class CreateEmptyTaskInLeadsWithoutOpenedTasksCommand
 * @package Command
 */
class CreateEmptyTaskInLeadsWithoutOpenedTasksCommand extends Command
{
    /** @var AmoCrmApiService  */
    private $amoCrmService;

    /**
     * CreateEmptyTaskInLeadsWithoutOpenedTasksCommand constructor.
     * @param AmoCrmApiService $service
     */
    public function __construct(AmoCrmApiService $service)
    {
        parent::__construct();
        $this->amoCrmService  = $service;
    }

    /**
     * Config
     */
    protected function configure()
    {
        $this
        ->setName('app:create-empty-task-in-leads')
        ->setDescription('Creates empty task in all leads without opened tasks');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool
     * @throws AuthorizeException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if(! $this->amoCrmService->authorize()) {
            throw new AuthorizeException();
        }
        $output->writeln('<info>Success authorization</info>');

        $leadsIds = $this->amoCrmService->getAllLeadsIds();
        $leadsIdsWithLinkedTasks = $this->amoCrmService->getLeadsIdsWithLinkedTasks();
        $leadsWithoutTasksIds = array_values(array_diff($leadsIds, $leadsIdsWithLinkedTasks));

        $output->writeln(sprintf('<comment>%d leads without tasks found<comment>', count($leadsWithoutTasksIds)));

        if(! $leadsWithoutTasksIds) {
            $output->writeln('<comment>No leads without tasks found<comment>');
            return 0;
        }

        $countOfAddedTasks = $this->amoCrmService->createEmptyTasksForLeads($leadsWithoutTasksIds, 'Сделка без задачи');
        if($countOfAddedTasks) {
            $output->writeln(sprintf('<info>%d tasks added</info>', count($countOfAddedTasks)));
        } else {
            $output->writeln('<comment>No tasks added<comment>');
        }

        return 0;
    }
}